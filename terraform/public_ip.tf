# Create public ip
module "public_ip" {
  source = "git::https://gitlab.com/azure-iac2/root-modules.git//public_ip"
  public_ip_name       = var.public_ip_name  
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  location             = data.azurerm_resource_group.rg.location
  allocation_method     = var.allocation_method
}